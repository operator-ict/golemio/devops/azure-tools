# Description

Azure tools helps manage files on Azure.

# Running the process

Go to the project folder in terminal and run `python app.py`.

# Example

## Download file

```bash
export AZURE_STORAGE_ACCOUNT=rabingolemio
export AZURE_ACCOUNT_KEY="XXXX"

python app.py --task download --container rabin-argo-tmp --url https://seznam.cz --key="pokus"
```

## Zip folder

```bash
app.py --task zip --key=pokus/2022-09-18
```

## Change tier
```bash
from Blob.main import AzureBlob
ab=AzureBlob("rabingolemio", "XXXX")
ab.set_tier_on_patern(container="my-bucket", prefix=None, path_pattern="*.zip", tier="cool")
```

## Zip folders
```bash
# example
nohup ./zip-folders.sh golem-input-gateway vehiclepositions 2023-07-24 2023-08-07 &
```
