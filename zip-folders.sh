#!/bin/bash

if [ "$#" -ne 4 ]; then
    echo "ERROR Wrong arguments - use command like: nohup $0 CONTAINER FOLDER_PATH START_DATE END_DATE &"
    exit 1
fi

d=$3
until [[ $d > $4 ]]; do
    python -u /app/app.py --task zip --container $1 --key $2/$d
    d=$(date -I -d "$d + 1 day")
done