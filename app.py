#!/usr/bin/env python3

import argparse
import json
import logging
import os
import sys
from datetime import date, timedelta, datetime

from Blob.main import AzureBlob

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)s] %(message)s',
    stream=sys.stdout,
)


def generate_cli_args():
    def valid_json(input_string):
        if input_string:
            try:
                return json.loads(input_string)
            except ValueError:
                msg = "Non-valid JSON: '{0}'.".format(input_string)
                raise argparse.ArgumentTypeError(msg)
        else:
            return None

    def valid_bool(input_string):
        if input_string.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        if input_string.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(description='Tools for Azure Blob storage file management')
    parser.add_argument('--task', type=str, required=True,
                        help='Task to perform', choices=['download', 'zip', 'status'])

    parser.add_argument('--container', type=str, help='Azure blob container', choices=None)
    parser.add_argument('--key', type=str, help='Key of file or folder')
    parser.add_argument('--url', type=str, help="Request URL (for download task only)", required=False)
    parser.add_argument('--filename', type=str, help="Filename (for download task only)", required=False,
                        default='')
    parser.add_argument('--suffix', type=str, help="Filename suffix (for download task only)", required=False,
                        default='.txt')
    parser.add_argument('--yesterday', type=valid_bool,
                        help="Zip files from `{key}/YYYY-MM-DD/` where YYYY-MM-DD is yesterday (for zip task only)",
                        required=False, default=False)
    parser.add_argument('--tier', type=str,
                        help="Azure blob tier for final zip folder (for zip task only)",
                        required=False, default='Cool')

    parser.add_argument('--request-method', type=str, help="Request method (for download task only)",
                        choices=['GET', 'POST'], default='GET')
    parser.add_argument('--request-header', type=valid_json, help="Request header (for download task only)",
                        required=False, default='')
    parser.add_argument('--request-body', type=valid_json, help="Request body (for download task only)", required=False,
                        default='')
    parser.add_argument('--request-other', type=valid_json,
                        help="Other arguments into Requests.request function (for download task only)", required=False,
                        default={})

    return parser.parse_args()


def load_env(key, default=None):
    if key in os.environ:
        return os.environ.get(key)
    if default is not None:
        return default
    raise ValueError('Key \'{}\' is not set as environment variable!'.format(key))


def prepare_folder_path(key: str, yesterday: bool):
    folder_path = key.strip("/")
    if yesterday:
        yesterday = date.today() - timedelta(days=1)
        folder_path = f"{folder_path}/{yesterday.strftime('%Y-%m-%d')}".strip("/")
    if len(folder_path) < 1:
        raise ValueError(f'Folder path \'{folder_path}\' based on args.key \'{key}\' is not valid folder in bucket!')
    return folder_path


if __name__ == '__main__':

    blob = AzureBlob(load_env("AZURE_STORAGE_ACCOUNT"), load_env("AZURE_ACCOUNT_KEY"))
    args = generate_cli_args()

    if args.task == 'download':
        file_name = args.filename if args.filename != "" else datetime.today().strftime("%Y-%m-%d/%H_%M_%S")
        blob.download_file_to_storage(
            args.container,
            url=args.url,
            file_name=f"{args.key}/{file_name}{args.suffix}",
            request_method=args.request_method,
            request_header=args.request_header,
            request_body=args.request_body,
            request_other=args.request_other
        )
    elif args.task == 'zip':
        blob.zip_folder(
            args.container,
            prepare_folder_path(args.key, args.yesterday),
            tier=args.tier
        )
    elif args.task == 'status':
        logging.warning("App is running!")
    else:
        print("Invalid arguments")
        sys.exit(1)
