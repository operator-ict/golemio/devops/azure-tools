import fnmatch
import json
import logging
import os
from datetime import datetime, timedelta

import requests
from azure.core.exceptions import HttpResponseError
from azure.storage.blob import ContainerClient, BlobServiceClient, generate_account_sas, ResourceTypes, \
    AccountSasPermissions
from stream_zip import ZIP_32, stream_zip

logger = logging.getLogger('azure')
logger.setLevel(logging.WARNING)


class AzureBlob:
    def __init__(self, storage_account_name, account_key):
        self.storage_account_name = storage_account_name
        self.account_key = account_key
        self.sas_token = generate_account_sas(
            account_name=self.storage_account_name,
            account_key=self.account_key,
            resource_types=ResourceTypes(service=True, object=True),
            permission=AccountSasPermissions(read=True, write=True, delete=True, list=True, create=True),
            expiry=datetime.utcnow() + timedelta(hours=1)
        )

        self.blob_service = BlobServiceClient(
            account_url=f'https://{self.storage_account_name}.blob.core.windows.net',
            credential=self.account_key
        )

    def download_file_to_storage(self, container, url, file_name, request_method, request_header={}, request_body='',
                                 request_other={}):
        container_client = self.blob_service.get_container_client(container=container)
        try:
            request_body = json.dumps(request_body) if request_body is not None else None
            r = requests.request(method=request_method, url=url, headers=request_header, data=request_body,
                                 **request_other)
            if r.status_code == 200:
                logging.info(
                    f"Downloading file: {url} to container: {container}/{file_name}")
                try:
                    container_client.upload_blob(name=file_name, data=r.content)
                except HttpResponseError as e:
                    logging.error(e)
                    return False
                return True
            logging.error(f'Bad request {url}. Reason: {r.reason}')
            return False
        except Exception as e:
            logging.error(e)
            return False

    def zip_folder(self, container, remote_folder_path, tier):
        container_client = self.blob_service.get_container_client(container=container)

        new_zip_filepath = f"{remote_folder_path}.zip"
        self._check_that_file_does_not_exists(container_client, new_zip_filepath)

        def blob_generator(objs):
            for obj in objs:
                yield obj['name'], obj['last_modified'], 0o600, ZIP_32, container_client.download_blob(
                    obj['name']).chunks()

        blob_list = [{"name": b.name, "last_modified": b.last_modified} for b in
                     container_client.list_blobs(name_starts_with=f'{remote_folder_path}/')]
        container_client.upload_blob(name=new_zip_filepath, data=stream_zip(blob_generator(blob_list)))
        container_client.set_standard_blob_tier_blobs(tier, str(new_zip_filepath))
        logging.info(f"Object {remote_folder_path} was uploaded.")
        # delete origin files
        for file in blob_list:
            container_client.delete_blob(file['name'])
            logging.info(f"Object {file['name']} was deleted.")

    def set_tier_on_patern(self, container: str, prefix: str = None, path_pattern: str = "*", tier: str = "cool"):
        container_client = self.blob_service.get_container_client(container=container)
        blob_list = container_client.list_blobs(prefix)
        for blob in blob_list:
            if fnmatch.fnmatchcase(blob.name, path_pattern):
                container_client.set_standard_blob_tier_blobs(tier, blob.name)

    @staticmethod
    def _check_that_file_does_not_exists(container_client, path):
        if container_client.get_blob_client(path).exists():
            raise ValueError(f'File {path} already exists!')

    @staticmethod
    def _add_file_to_zip(remote_file_path, remote_folder_path, tmp_dir, container_client: ContainerClient, zipf):
        object_name = remote_file_path['name'][len(remote_folder_path + '/'):]
        if len(object_name) == 0 or object_name.endswith('/'):
            return
        local_path = os.path.join(tmp_dir, object_name)
        dir_path = os.path.dirname(local_path)
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)

        with open(local_path, "wb") as local_blob:
            local_blob.write(container_client.download_blob(remote_file_path).readall())
        zipf.write(local_path, object_name)
        os.unlink(local_path)
