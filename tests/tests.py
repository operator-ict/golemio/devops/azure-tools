import logging
import os
import random
import string
import sys
import unittest

from Blob.main import AzureBlob
from app import prepare_folder_path

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.StreamHandler(sys.stdout)
    ]
)


class TestDownload(unittest.TestCase):
    TEST_FILE_KEY = '{}/TEST/test.txt'.format("".join([random.choice(string.ascii_letters) for _ in range(10)]))
    TEST_URL = 'https://seznam.cz'
    TEST_REQUEST_METHOD = 'POST'

    def setUp(self):
        if all([os.getenv("AZURE_STORAGE_ACCOUNT"), os.getenv("AZURE_ACCOUNT_KEY"), os.getenv('AZURE_CONTAINER')]):
            self.azure = AzureBlob(os.getenv("AZURE_STORAGE_ACCOUNT"),
                                   os.getenv("AZURE_ACCOUNT_KEY"))
            self.TEST_CONTAINER = os.getenv("AZURE_CONTAINER")
        else:
            logging.error('ENV VARIABLES not defined.')

    def test_download_and_zip(self):
        # Download test file from sample API using `s3Tools.download_file_to_s3`
        logging.info(f'Attempting to download file from {self.TEST_URL} to {self.TEST_FILE_KEY} ... ')
        self.assertTrue(
            self.azure.download_file_to_storage(self.TEST_CONTAINER, self.TEST_URL, self.TEST_FILE_KEY,
                                                self.TEST_REQUEST_METHOD)
        )

        container_client = self.azure.blob_service.get_container_client(self.TEST_CONTAINER)

        # Confirm existence
        with self.assertRaises(ValueError):
            self.azure._check_that_file_does_not_exists(container_client, self.TEST_FILE_KEY)

        # Try zipping the parent folder
        logging.info('Zipping parent directory of test file ...')
        folder_path = '/'.join(self.TEST_FILE_KEY.split('/')[:-1])
        self.azure.zip_folder(self.TEST_CONTAINER, folder_path, tier='Hot')
        logging.info('Zipping succesful.')

        # Confirm zipped file
        with self.assertRaises(ValueError):
            self.assertTrue(self.azure._check_that_file_does_not_exists(container_client, folder_path + '.zip'))
        self.assertFalse(self.azure._check_that_file_does_not_exists(container_client, self.TEST_FILE_KEY))

        # Delete zipped test file
        logging.info('Deleting zip file.')
        container_client.delete_blob(folder_path + '.zip')

        # Confirm deletion
        self.assertFalse(self.azure._check_that_file_does_not_exists(container_client, folder_path + '.zip'))

    def test_invalid_folder_path(self):
        self.assertRaises(ValueError, prepare_folder_path, "", False)
        self.assertRaises(ValueError, prepare_folder_path, "/", False)
        self.assertRaises(ValueError, prepare_folder_path, "///", False)

    def tearDown(self):
        del self.azure
