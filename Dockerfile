FROM python:3.11.4-slim-bookworm

WORKDIR /app

# owner is root
COPY mycron /etc/cron.d/mycron

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    cron \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* && \
    groupadd --system -g 1001 nonroot && \
    useradd --system --base-dir /app --uid 1001 --gid nonroot nonroot && \
    chown -R nonroot /app && \
    chmod 644 /etc/cron.d/mycron && \
    chmod u+s /usr/sbin/cron && \
    chmod 666 /etc/environment

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY --chown=nonroot:nonroot . .
USER nonroot

CMD ["./docker-start.sh"]
